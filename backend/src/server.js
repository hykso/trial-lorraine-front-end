// Require dependencies for backend
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

// Initialize express app and use dependencies
const app = express();
app.use(cors());
app.use(bodyParser.json());

// Initialize AirTable
const Airtable = require("airtable");
Airtable.configure({
  endpointUrl: "https://api.airtable.com",
  apiKey: "YOUR API KEY HERE"
});

const base = require("airtable").base("appv72N3b1bR61g7T");

app.get("/workouts", async function(req, res) {
  let workouts = [];

  await base("Workouts")
    .select({
      view: "Grid view",
      fields: [
        "DateTime",
        "Level",
        "Rounds",
        "Trainer",
        "Filming Date",
        "Full",
        "Time",
        "flatten_date"
      ]
    })
    .eachPage(
      function page(records, fetchNextPage) {
        records.forEach(function(record) {
          let workout = ("Retrieved", record);

          if(workout.fields['Filming Date']) {
            workouts.push(workout._rawJson);
          }
        });

        fetchNextPage();
      }
  )

  res.send(workouts);
});


app.get("/schedules", async function(req, res) {
  let filmingDates = [];

  await base("Schedules")
    .select({
      view: "Grid view"
    })
    .eachPage(
      function page(records, fetchNextPage) {
        records.forEach(function(record) {
          let filmingDate = ("Retrieved", record);
          filmingDates.push(filmingDate);
        });
        fetchNextPage();
      }
    );

  res.send(filmingDates);
});

app.post("/reservations/", function(req, res) {
  let reservationArray = [
    {
      fields: {
        Name: req.body.name,
        Email: req.body.email,
        "Phone Number": req.body.phoneNumber,
        Workout: req.body.workout
      }
    }
  ];

  base("Reservations").create(reservationArray, function(err, records) {
    if (err) {
      res.send(err);
    }
    records.forEach(function(record) {
      // TODO fix this endpoint to return 200 and body message
      res.send({status: 200, body: record});
    });
  });
});

const port = process.env.PORT || 8082;

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
