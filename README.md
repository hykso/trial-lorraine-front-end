# FightCamp Studio Front End Trial

To maintain our service to our current customers, FightCamp needs to film and release around 12 workouts every week. These workouts are usually filmed in the FightCamp Studio in Costa Mesa in batches of 4 to 6 in a single day and require around 10 participants for each class.

The participants usually get notified that a new schedule is ready and then can go on https://studio.joinfightcamp.com to book their spot for the desired workout. They select the workout and enter their information to complete the reservation. The Community Manager makes sure there is enough participants for each workout, but also not too many. That's why a workout can be grayed-out for the list.

The goal of this trial is to recreate the Schedules and Reservation components using Vue as the Frontend Framework, Node/Express (already built) and Airtable as a "Database/CMS".

## Architecture

There are 3 main parts to this project:

**Frontend**

   1. Fetch the upcoming `Schedules` with their associated `Workouts` from the backend server and populate the workout components.

   2. When a user press the 'Reserve', a modal appears to gather the user's information and sends it to the backend server to reserve a spot.

**Node/Express Backend**

   1. Retrieves the workouts available from Airtable

   2. Create new reservations in Airtable.

**Airtable as a "SaaS database"**

   1. The Community Manager manages the upcoming `Schedules` and the `Workouts` in Airtable

   2. The Community Manager can see the reservations for each workout.

   3. If a workout seems to have enough reservations, the Community manager can "close" the workout manually preventing any more reservations for that `Workout`.

![MVIMG_20190801_175259](/.readme/MVIMG_20190801_175259.jpg)

## 1. Frontend

The frontend is separated into 3 views.

1. Schedules
2. Reservation Form
3. Confirmation

### Schedules

The Schedules view displays a list of workouts grouped by their associated Schedule. The user can navigate from one `Schedule` to another, but only one `Schedule` is shown at a time.

![Screen Shot 2019-08-01 at 4.05.28 PM](/.readme/image1.png)

Here are more details on what the workout cell looks like. The information presented here is:
- Time (1:15pm, 2:35pm ...)
- Duration in minutes (30 minutes, 40 minutes, 50 minutes)
- Difficulty of the workout (Open, Intermediate)
- Number of Rounds
- Name of the trainer
- Picture of the trainer

![Screen Shot 2019-08-01 at 3.59.11 PM](/.readme/image2.png)

Here's what the workout cell looks like when canceled.

![Screen Shot 2019-08-01 at 3.59.11 PM](/.readme/image5.png)

### Reservation Form

Once the user has selected a `Workout`, a form is shown with the workout's information and fields to enter the following information:

- Full Name
- Email `foo@joinfightcamp.com`
- Phone Number `(123) 456-7890`

The user can only reserve a spot when all the fields are filled out and are formatted correctly. Once the user reserves their spot, the information should be sent to the backend. The user can go back to the schedules at any time, unless the reservation is being processed.

![Screen Shot 2019-08-01 at 3.59.44 PM](/.readme/image3.png)

### Confirmation

When the reservation has been processed successfully, a small message is displayed about the reserved workout

![Screen Shot 2019-08-01 at 4.02.58 PM](/.readme/image4.png)

## 2. Backend

3 endpoints have been created for you on the backend.

1.  `GET /schedules`
2.  `GET /workouts`
3.  `POST /reservations`

### `GET /schedules`

Retrieves the available `Schedules` from the Airtable table.

### `GET /workouts`

Retrieves the available `Workouts` from Airtable.

### `POST /reservations`

Creates a new `Reservation` with the user information for the selected workout's information.

## Other Directives

- No CSS framework
- No UI plugins or libraries other than the ones currently installed
- Performance and accessibility best practices are preferred
- Use VueX for state management
- Please write some specs. Use jest, cypress or any other testing tools that you prefer
- Do not change the backend code
- Do not change the structure of the json files

## What will be evaluated

- Use of Vue features for the right scenarios
- Responsiveness and Aesthetics of the UI
- Code Readability and consistency
- Proper segmentation of the code

## Quick tips
- Don't be afraid to ask questions if you are not sure about certain aspects of the trial.
- Make sure to breakdown the project into smaller parts. It's okay if the project is not completed at the end of the trial, we don't expect it to be with the short timeframe, but being able to demo working features will help during the evaluation.


## Getting Started

This repository has 3 folders:

- `assets` -> A few files that can be useful for the project

- `frontend` -> Starter Vue project using Vue CLI. 
  - Run `npm install` to get all dependencies. 
  - Run `npm run serve` to start a local development server. 
  - If another framework is used, you can delete this folder and start over.

- `backend` -> Starter for Express server, listening on port 8082. 
  - Run `npm install` to get all dependencies. 
  - Add your Airtable API key to `server.js`. 
  - Run `npm start` to start a local development server for the API you will use. 

Here's an invitation to a prepopulated Airtable. https://airtable.com/invite/l?inviteId=inv5SA2FbCuV4PCpA&inviteToken=3918bfbee50fe26f99d91d5fe342fb8d01e43463047e3349ff29fba942071a9a

## Project submission

Use Git to commit your code during your trial and push your solution to the this repository.
